import * as firebase from "firebase";
import "firebase/database";

let config = {
    apiKey: "AIzaSyCWtVNX-PF0nDO7HumaEHH-_4LDhG7k2iE",
  authDomain: "djjumoflow-88e5f.firebaseapp.com",
  databaseURL: "https://djjumoflow-88e5f-default-rtdb.firebaseio.com",
  projectId: "djjumoflow-88e5f",
  storageBucket: "djjumoflow-88e5f.appspot.com",
  messagingSenderId: "768153236361",
  appId: "1:768153236361:web:7774821ea655be13832758",
  measurementId: "G-SX5791NZMY"
};

firebase.initializeApp(config);

export default firebase.database();